var log = console.log.bind(console) // eslint-disable-line no-console
var sel = document.querySelector.bind(document)

var h = hyperapp.h
var app = hyperapp.app

app({
  root: sel('#container'),
  state: 'Hi!',
  view: function (state) {
    return h('h1', null, state)
  }
})
log('Start!')
